#include "Character/Animation/Notify/AnimNotifyGroundedEntryState.h"
#include "Character/Animation/CharacterAnimInstance.h"

void UAnimNotifyGroundedEntryState::Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation)
{
	UCharacterAnimInstance* AnimIns = Cast<UCharacterAnimInstance>(MeshComp->GetAnimInstance());
	if (AnimIns)
	{
		AnimIns->SetGroundedEntryState(GroundedEntryState);
	}
}

FString UAnimNotifyGroundedEntryState::GetNotifyName_Implementation() const
{
	FString Name(TEXT("Grounded Entry State: "));
	Name.Append(GetEnumerationToString(GroundedEntryState));
	return Name;
}
