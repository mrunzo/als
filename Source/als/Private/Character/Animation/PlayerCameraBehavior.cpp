#include "Character/Animation/PlayerCameraBehavior.h"

void UPlayerCameraBehavior::SetRotationMode(ERotationMode RotationModes)
{
	bVelocityDirection = RotationMode == ERotationMode::VelocityDirection;
	bLookingDirection = RotationMode == ERotationMode::LookingDirection;
	bAiming = RotationMode == ERotationMode::Aiming;
	//Set RotaionMode
	RotationMode = RotationModes;
}

void UPlayerCameraBehavior::SetRightShoulder(bool bNewRightShoulder)
{
	bRightShoulder = bNewRightShoulder;
}

void UPlayerCameraBehavior::NativeInitializeAnimation()
{
	Super::NativeInitializeAnimation();

		// Set the Camera Model
		if (CameraModel.IsNull())
		{
			
		}
		else 
		{
			SetCameraModel();
		}
		
}

void UPlayerCameraBehavior::SetCameraModel()
{
	const FString ContextString = GetFullName();
	FCameraStateSettings* OutRow =
		CameraModel.DataTable->FindRow<FCameraStateSettings>(CameraModel.RowName, ContextString);
	check(OutRow);
	CameraData = *OutRow;
}
