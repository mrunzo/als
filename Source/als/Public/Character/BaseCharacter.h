#pragma once

#include "CoreMinimal.h"
#include "Components/TimelineComponent.h"
#include "Library/CharacterEnumLibrary.h"
#include "Library/CharacterStructLibrary.h"
#include "Engine/DataTable.h"
#include "GameFramework/Character.h"
#include "BaseCharacter.generated.h"

// forward declarations
class UDebugComponent;
class ABasePlayerController;
class UTimelineComponent;
class UAnimInstance;
class UAnimMontage;
class UCharacterAnimInstance;
class UPlayerCameraBehavior;
enum class EVisibilityBasedAnimTickOption : uint8;

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FJumpPressedSignature);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FRagdollStateChangedSignature, bool, bRagdollState);

UCLASS(BlueprintType)
class ALS_API ABaseCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	ABaseCharacter(const FObjectInitializer& ObjectInitializer);

	UFUNCTION(BlueprintCallable, Category = "Movement")
		FORCEINLINE class UPlayerCharacterMovementComponent* GetMyMovementComponent() const
	{
		return MyCharacterMovementComponent;
	}

	virtual void Tick(float DeltaTime) override;

	virtual void BeginPlay() override;

	virtual void PreInitializeComponents() override;

	virtual void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) override;

	virtual void PostInitializeComponents() override;

	//virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	UPROPERTY(BlueprintReadOnly, Category = "Utility")
		UCharacterAnimInstance* MainAnimInstance = nullptr;

	UPROPERTY(BlueprintReadOnly, Category = "Camera")
    UPlayerCameraBehavior* CameraBehavior;

	/** Ragdoll System */

	/** Implement on BP to get required get up animation according to character's state */
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "Ragdoll System")
		UAnimMontage* GetGetUpAnimation(bool bRagdollFaceUpState);

	UFUNCTION(BlueprintCallable, Category = "Ragdoll System")
		virtual void RagdollStart();

	UFUNCTION(BlueprintCallable, Category = "Ragdoll System")
		virtual void RagdollEnd();

	/** Character States */

	UFUNCTION(BlueprintCallable, Category = "Character States")
		void SetMovementState(EMovementState NewState, bool bForce = false);

	UFUNCTION(BlueprintGetter, Category = "Character States")
		EMovementState GetMovementState() const { return MovementState; }

	UFUNCTION(BlueprintGetter, Category = "Character States")
		EMovementState GetPrevMovementState() const { return PrevMovementState; }

	UFUNCTION(BlueprintCallable, Category = "Character States")
		void SetMovementAction(EMovementAction NewAction, bool bForce = false);

	UFUNCTION(BlueprintGetter, Category = "Character States")
		EMovementAction GetMovementAction() const { return MovementAction; }

	UFUNCTION(BlueprintCallable, Category = "Character States")
		void SetStance(EStance NewStance, bool bForce = false);

	UFUNCTION(BlueprintGetter, Category = "Character States")
		EStance GetStance() const { return Stance; }

	UFUNCTION(BlueprintCallable, Category = "Character States")
		void SetGait(EGait NewGait, bool bForce = false);

	UFUNCTION(BlueprintGetter, Category = "Character States")
		EGait GetGait() const { return Gait; }

	UFUNCTION(BlueprintGetter, Category = "CharacterStates")
		EGait GetDesiredGait() const { return DesiredGait; }

	UFUNCTION(BlueprintCallable, Category = "Character States")
		void SetRotationMode(ERotationMode NewRotationMode, bool bForce = false);

	UFUNCTION(BlueprintGetter, Category = "Character States")
		ERotationMode GetRotationMode() const { return RotationMode; }

	UFUNCTION(BlueprintCallable, Category = "Character States")
		void SetViewMode(EViewMode NewViewMode, bool bForce = false);

	UFUNCTION(BlueprintGetter, Category = "Character States")
		EViewMode GetViewMode() const { return ViewMode; }

	UFUNCTION(BlueprintCallable, Category = "Character States")
		void SetOverlayState(EOverlayState NewState, bool bForce = false);

	UFUNCTION(BlueprintGetter, Category = "Character States")
		EOverlayState GetOverlayState() const { return OverlayState; }

	/** Landed, Jumped, Rolling, Mantling and Ragdoll*/
	/** On Landed*/
	UFUNCTION(BlueprintCallable, Category = "Character States")
		void EventOnLanded();

	/** Input */

	UPROPERTY(BlueprintAssignable, Category = "Input")
		FJumpPressedSignature JumpPressedDelegate;

	UPROPERTY(BlueprintAssignable, Category = "Input")
		FRagdollStateChangedSignature RagdollStateChangedDelegate;

	UFUNCTION(BlueprintGetter, Category = "Input")
		EStance GetDesiredStance() const { return DesiredStance; }

	UFUNCTION(BlueprintSetter, Category = "Input")
		void SetDesiredStance(EStance NewStance);

	UFUNCTION(BlueprintCallable, Category = "Character States")
		void SetDesiredGait(EGait NewGait);

	UFUNCTION(BlueprintGetter, Category = "Input")
		ERotationMode GetDesiredRotationMode() const { return DesiredRotationMode; }

	UFUNCTION(BlueprintSetter, Category = "Input")
		void SetDesiredRotationMode(ERotationMode NewRotMode);

	UFUNCTION(BlueprintCallable, Category = "Input")
		FVector GetPlayerMovementInput() const;

	/** Rotation System */

	UFUNCTION(BlueprintCallable, Category = "Rotation System")
		void SetActorLocationAndTargetRotation(FVector NewLocation, FRotator NewRotation);

	/** Movement System */

	UFUNCTION(BlueprintGetter, Category = "Movement System")
		bool HasMovementInput() const { return bHasMovementInput; }

	UFUNCTION(BlueprintCallable, Category = "Movement System")
		void SetHasMovementInput(bool bNewHasMovementInput);

	UFUNCTION(BlueprintCallable, Category = "Movement System")
		FMovementSettings GetTargetMovementSettings() const;

	UFUNCTION(BlueprintCallable, Category = "Movement System")
		EGait GetAllowedGait() const;

	UFUNCTION(BlueprintCallable, Category = "Movement System")
		EGait GetActualGait(EGait AllowedGait) const;

	UFUNCTION(BlueprintCallable, Category = "Movement System")
		bool CanSprint() const;

	/** BP implementable function that called when Breakfall starts */
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Movement System")
		void OnBreakfall();
	virtual void OnBreakfall_Implementation();

	/** BP implementable function that called when A Montage starts, e.g. during rolling */
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Movement System")
		void PlayMontage(UAnimMontage* Montage, float PlayRate);
	virtual void PlayMontage_Implementation(UAnimMontage* Montage, float PlayRate);

	/** Implement on BP to get required roll animation according to character's state */
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "Movement System")
		UAnimMontage* GetRollAnimation();

	/** Utility */

	UFUNCTION(BlueprintCallable, Category = "Utility")
		UCharacterAnimInstance* GetMainAnimInstance() { return MainAnimInstance; }

	UFUNCTION(BlueprintCallable, Category = "Utility")
		float GetAnimCurveValue(FName CurveName) const;

	UFUNCTION(BlueprintCallable, Category = "Utility")
		void SetVisibleMesh(USkeletalMesh* NewSkeletalMesh);

	/** Camera System */

	UFUNCTION(BlueprintCallable, Category = "Camera System")
		void SetCameraBehavior(UPlayerCameraBehavior* CamBeh) { CameraBehavior = CamBeh; }

	UFUNCTION(BlueprintCallable, Category = "Camera System")
		virtual ECollisionChannel GetThirdPersonTraceParams(FVector& TraceOrigin, float& TraceRadius);

	UFUNCTION(BlueprintCallable, Category = "Camera System")
		virtual FTransform GetThirdPersonPivotTarget();

	UFUNCTION(BlueprintCallable, Category = "Camera System")
		virtual FVector GetFirstPersonCameraTarget();

	/** Essential Information Getters/Setters */

	UFUNCTION(BlueprintGetter, Category = "Essential Information")
		FVector GetAcceleration() const { return Acceleration; }

	UFUNCTION(BlueprintCallable, Category = "Essential Information")
		void SetAcceleration(const FVector& NewAcceleration);

	UFUNCTION(BlueprintGetter, Category = "Essential Information")
		bool IsMoving() const { return bIsMoving; }

	UFUNCTION(BlueprintCallable, Category = "Essential Information")
		void SetIsMoving(bool bNewIsMoving);

	UFUNCTION(BlueprintCallable, Category = "Essential Information")
		FVector GetMovementInput() const;

	UFUNCTION(BlueprintGetter, Category = "Essential Information")
		float GetMovementInputAmount() const { return MovementInputAmount; }

	UFUNCTION(BlueprintCallable, Category = "Essential Information")
		void SetMovementInputAmount(float NewMovementInputAmount);

	UFUNCTION(BlueprintGetter, Category = "Essential Information")
		float GetSpeed() const { return Speed; }

	UFUNCTION(BlueprintCallable, Category = "Essential Information")
		void SetSpeed(float NewSpeed);

	UFUNCTION(BlueprintCallable, Category = "Essential Information")
		FRotator GetAimingRotation() const { return AimingRotation; }

	UFUNCTION(BlueprintGetter, Category = "Essential Information")
		float GetAimYawRate() const { return AimYawRate; }

	UFUNCTION(BlueprintCallable, Category = "Essential Information")
		void SetAimYawRate(float NewAimYawRate);

	UFUNCTION(BlueprintCallable, Category = "Essential Information")
		void GetControlForwardRightVector(FVector& Forward, FVector& Right) const;

protected:
	/** Ragdoll System */

	void RagdollUpdate(float DeltaTime);

	void SetActorLocationDuringRagdoll(float DeltaTime);

	/** State Changes */

	virtual void OnMovementModeChanged(EMovementMode PrevMovementMode, uint8 PreviousCustomMode = 0) override;

	virtual void OnMovementStateChanged(EMovementState PreviousState);

	virtual void OnMovementActionChanged(EMovementAction PreviousAction);

	virtual void OnStanceChanged(EStance PreviousStance);

	virtual void OnRotationModeChanged(ERotationMode PreviousRotationMode);

	virtual void OnGaitChanged(EGait PreviousGait);

	virtual void OnViewModeChanged(EViewMode PreviousViewMode);

	virtual void OnOverlayStateChanged(EOverlayState PreviousState);

	virtual void OnVisibleMeshChanged(const USkeletalMesh* PreviousSkeletalMesh);

	virtual void OnStartCrouch(float HalfHeightAdjust, float ScaledHalfHeightAdjust) override;

	virtual void OnEndCrouch(float HalfHeightAdjust, float ScaledHalfHeightAdjust) override;

	virtual void Landed(const FHitResult& Hit) override;

	void OnLandFrictionReset();

	void SetEssentialValues(float DeltaTime);

	void UpdateCharacterMovement();

	void UpdateGroundedRotation(float DeltaTime);

	void UpdateInAirRotation(float DeltaTime);

	/** Utils */

	void SmoothCharacterRotation(FRotator Target, float TargetInterpSpeed, float ActorInterpSpeed, float DeltaTime);

	float CalculateGroundedRotationRate() const;

	void LimitRotation(float AimYawMin, float AimYawMax, float InterpSpeed, float DeltaTime);

	void SetMovementModel();

	void ForceUpdateCharacterState();

	/** Input */

	void PlayerForwardMovementInput(float Value);

	void PlayerRightMovementInput(float Value);

	void PlayerCameraUpInput(float Value);

	void PlayerCameraRightInput(float Value);

	void JumpPressedAction();

	void JumpReleasedAction();

	void SprintPressedAction();

	void SprintReleasedAction();

	void AimPressedAction();

	void AimReleasedAction();

	void CameraPressedAction();

	void CameraReleasedAction();

	void OnSwitchCameraMode();

	void StancePressedAction();

	void WalkPressedAction();

	void RagdollPressedAction();

	void VelocityDirectionPressedAction();

	void LookingDirectionPressedAction();

protected:
	/* Custom movement component*/
	UPROPERTY()
		UPlayerCharacterMovementComponent* MyCharacterMovementComponent;

	/** Input */

	UPROPERTY(EditAnywhere, Replicated, BlueprintReadWrite, Category = "Input")
		ERotationMode DesiredRotationMode = ERotationMode::LookingDirection;

	UPROPERTY(EditAnywhere, Replicated, BlueprintReadWrite, Category = "Input")
		EGait DesiredGait = EGait::Running;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated, Category = "Input")
		EStance DesiredStance = EStance::Standing;

	UPROPERTY(EditDefaultsOnly, Category = "Input", BlueprintReadOnly)
		float LookUpDownRate = 1.25f;

	UPROPERTY(EditDefaultsOnly, Category = "Input", BlueprintReadOnly)
		float LookLeftRightRate = 1.25f;

	UPROPERTY(EditDefaultsOnly, Category = "Input", BlueprintReadOnly)
		float RollDoubleTapTimeout = 0.3f;

	UPROPERTY(EditDefaultsOnly, Category = "Input", BlueprintReadOnly)
		float ViewModeSwitchHoldTime = 0.2f;

	UPROPERTY(Category = "Input", BlueprintReadOnly)
		int32 TimesPressedStance = 0;

	UPROPERTY(Category = "Input", BlueprintReadOnly)
		bool bBreakFall = false;

	UPROPERTY(Category = "Input", BlueprintReadOnly)
		bool bSprintHeld = false;

	/** State Values */

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "State Values")
		EOverlayState OverlayState = EOverlayState::Default;

	/** Movement System */

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Movement System")
		FDataTableRowHandle MovementModel;

	/** Essential Information */

	UPROPERTY(BlueprintReadOnly, Category = "Essential Information")
		FVector Acceleration = FVector::ZeroVector;

	UPROPERTY(BlueprintReadOnly, Category = "Essential Information")
		bool bIsMoving = false;

	UPROPERTY(BlueprintReadOnly, Category = "Essential Information")
		bool bHasMovementInput = false;

	UPROPERTY(BlueprintReadOnly, Category = "Essential Information")
		FRotator LastVelocityRotation;

	UPROPERTY(BlueprintReadOnly, Category = "Essential Information")
		FRotator LastMovementInputRotation;

	UPROPERTY(BlueprintReadOnly, Category = "Essential Information")
		float Speed = 0.0f;

	UPROPERTY(BlueprintReadOnly, Category = "Essential Information")
		float MovementInputAmount = 0.0f;

	UPROPERTY(BlueprintReadOnly, Category = "Essential Information")
		float AimYawRate = 0.0f;

	/** Replicated Essential Information*/

	UPROPERTY(BlueprintReadOnly, Category = "Essential Information")
		float EasedMaxAcceleration = 0.0f;

	UPROPERTY(BlueprintReadOnly, Category = "Essential Information")
		FVector CurrentAcceleration = FVector::ZeroVector;

	UPROPERTY(BlueprintReadOnly, Category = "Essential Information")
		FRotator ControlRotation = FRotator::ZeroRotator;

	/** Replicated Skeletal Mesh Information*/
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Skeletal Mesh")
		USkeletalMesh* VisibleMesh = nullptr;

	/** State Values */

	UPROPERTY(BlueprintReadOnly, Category = "State Values")
		EMovementState MovementState = EMovementState::None;

	UPROPERTY(BlueprintReadOnly, Category = "State Values")
		EMovementState PrevMovementState = EMovementState::None;

	UPROPERTY(BlueprintReadOnly, Category = "State Values")
		EMovementAction MovementAction = EMovementAction::None;

	UPROPERTY(BlueprintReadOnly, Category = "State Values")
		ERotationMode RotationMode = ERotationMode::LookingDirection;

	UPROPERTY(BlueprintReadOnly, Category = "State Values")
		EGait Gait = EGait::Walking;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "State Values")
		EStance Stance = EStance::Standing;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "State Values")
		EViewMode ViewMode = EViewMode::ThirdPerson;

	/** Movement System */

	UPROPERTY(BlueprintReadOnly, Category = "Movement System")
		FMovementStateSettings MovementData;

	/** Rotation System */

	UPROPERTY(BlueprintReadOnly, Category = "Rotation System")
		FRotator TargetRotation = FRotator::ZeroRotator;

	UPROPERTY(BlueprintReadOnly, Category = "Rotation System")
		FRotator InAirRotation = FRotator::ZeroRotator;

	UPROPERTY(BlueprintReadOnly, Category = "Rotation System")
		float YawOffset = 0.0f;

	/** Breakfall System */

	/** If player hits to the ground with a specified amount of velocity, switch to breakfall state */
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Breakfall System")
		bool bBreakfallOnLand = true;

	/** If player hits to the ground with an amount of velocity greater than specified value, switch to breakfall state */
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Breakfall System", meta = (EditCondition =
		"bBreakfallOnLand"))
		float BreakfallOnLandVelocity = 600.0f;

	/** Ragdoll System */

	/** If the skeleton uses a reversed pelvis bone, flip the calculation operator */
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Ragdoll System")
		bool bReversedPelvis = false;

	/** If player hits to the ground with a specified amount of velocity, switch to ragdoll state */
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Ragdoll System")
		bool bRagdollOnLand = false;

	/** If player hits to the ground with an amount of velocity greater than specified value, switch to ragdoll state */
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Ragdoll System", meta = (EditCondition =
		"bRagdollOnLand"))
		float RagdollOnLandVelocity = 1000.0f;

	UPROPERTY(BlueprintReadOnly, Category = "Ragdoll System")
		bool bRagdollOnGround = false;

	UPROPERTY(BlueprintReadOnly, Category = "Ragdoll System")
		bool bRagdollFaceUp = false;

	UPROPERTY(BlueprintReadOnly, Category = "Ragdoll System")
		FVector LastRagdollVelocity = FVector::ZeroVector;

	UPROPERTY(BlueprintReadOnly, Replicated, Category = "Ragdoll System")
		FVector TargetRagdollLocation = FVector::ZeroVector;

	/** Cached Variables */

	FVector PreviousVelocity = FVector::ZeroVector;

	float PreviousAimYaw = 0.0f;

	/** Last time the 'first' crouch/roll button is pressed */
	float LastStanceInputTime = 0.0f;

	/** Last time the camera action button is pressed */
	float CameraActionPressedTime = 0.0f;

	/* Timer to manage camera mode swap action */
	FTimerHandle OnCameraModeSwapTimer;

	/* Timer to manage reset of braking friction factor after on landed event */
	FTimerHandle OnLandedFrictionResetTimer;

	/* Smooth out aiming by interping control rotation*/
	FRotator AimingRotation = FRotator::ZeroRotator;

private:
	UPROPERTY()
		UDebugComponent* DebugComponent = nullptr;

public:
	UPROPERTY(BlueprintReadOnly, Category = "References")
		ABasePlayerController* BasePlayerController = nullptr;

};
