#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/Tasks/BTTask_BlackboardBase.h"
#include "BTTask_SetFocusToPlayer.generated.h"

/** Set AIController's Focus to the Player's Pawn Actor. */
UCLASS(Category = ALS, meta = (DisplayName = "Set Focus to Player"))
class ALS_API UBTTask_SetFocusToPlayer : public UBTTaskNode
{
	GENERATED_BODY()

public:
	UBTTask_SetFocusToPlayer();

	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
	virtual FString GetStaticDescription() const override;
};
