#pragma once

#include "CoreMinimal.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Components/ActorComponent.h"
#include "CollisionShape.h"
#include "DebugComponent.generated.h"

class ABaseCharacter;
class USkeletalMesh;

UCLASS(Blueprintable, BlueprintType)
class ALS_API UDebugComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UDebugComponent();

	virtual void TickComponent(float DeltaTime, ELevelTick TickType,
		FActorComponentTickFunction* ThisTickFunction) override;

	virtual void OnComponentDestroyed(bool bDestroyingHierarchy) override;


	// utility functions to draw trace debug shapes,
	// which are derived from Engine/Private/KismetTraceUtils.h.
	// Sadly the functions are private, which was the reason
	// why there reimplemented here.
	static void DrawDebugLineTraceSingle(const UWorld* World,
		const FVector& Start,
		const FVector& End,
		EDrawDebugTrace::Type DrawDebugType,
		bool bHit,
		const FHitResult& OutHit,
		FLinearColor TraceColor,
		FLinearColor TraceHitColor,
		float DrawTime);

	static void DrawDebugCapsuleTraceSingle(const UWorld* World,
		const FVector& Start,
		const FVector& End,
		const FCollisionShape& CollisionShape,
		EDrawDebugTrace::Type DrawDebugType,
		bool bHit,
		const FHitResult& OutHit,
		FLinearColor TraceColor,
		FLinearColor TraceHitColor,
		float DrawTime);

	static void DrawDebugSphereTraceSingle(const UWorld* World,
		const FVector& Start,
		const FVector& End,
		const FCollisionShape& CollisionShape,
		EDrawDebugTrace::Type DrawDebugType,
		bool bHit,
		const FHitResult& OutHit,
		FLinearColor TraceColor,
		FLinearColor TraceHitColor,
		float DrawTime);

protected:
	virtual void BeginPlay() override;

public:
	UPROPERTY(BlueprintReadOnly, Category = "Debug")
		ABaseCharacter* OwnerCharacter;

};

