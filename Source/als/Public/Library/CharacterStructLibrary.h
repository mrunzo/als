#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Materials/MaterialInterface.h"
#include "Library/CharacterEnumLibrary.h"

#include "CharacterStructLibrary.generated.h"

class UCurveVector;
class UAnimMontage;
class UAnimSequenceBase;
class UCurveFloat;
class UNiagaraSystem;
class UMaterialInterface;
class USoundBase;
class UPrimitiveComponent;

USTRUCT(BlueprintType)
struct FComponentAndTransform
{
	GENERATED_BODY()

		UPROPERTY(EditAnywhere, Category = "Character Struct Library")
		FTransform Transform;

	UPROPERTY(EditAnywhere, Category = "Character Struct Library")
		UPrimitiveComponent* Component = nullptr;
};

USTRUCT(BlueprintType)
struct FCameraSettings
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Camera")
		FVector CameraOffset;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Camera")
		FVector PivotLagSpeed;
};

USTRUCT(BlueprintType)
struct FCameraGaitSettings
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Camera")
		float RotationLagSpeed = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Camera")
		FVector N_PivotOffset;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Camera")
		FCameraSettings N_Walking;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Camera")
		FCameraSettings N_Running;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Camera")
		FCameraSettings N_Sprinting;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Camera")
		FVector CLF_PivotOffset;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Camera")
		FCameraSettings CLF_Walking;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Camera")
		FCameraSettings CLF_Running;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Camera")
		FCameraSettings CLF_Sprinting;
};

USTRUCT(BlueprintType)
struct FCameraStateSettings : public FTableRowBase
{
	GENERATED_BODY()
		UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Camera")
		float TP_FOV = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Camera")
		float FP_FOV = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Camera")
		FCameraGaitSettings VelocityDirection;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Camera")
		FCameraGaitSettings LookingDirection;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Camera")
		FCameraGaitSettings Aiming;
};

USTRUCT(BlueprintType)
struct FMantleAsset
{
	GENERATED_BODY()

		UPROPERTY(EditAnywhere, Category = "Mantle System")
		UAnimMontage* AnimMontage = nullptr;

	UPROPERTY(EditAnywhere, Category = "Mantle System")
		UCurveVector* PositionCorrectionCurve = nullptr;

	UPROPERTY(EditAnywhere, Category = "Mantle System")
		FVector StartingOffset;

	UPROPERTY(EditAnywhere, Category = "Mantle System")
		float LowHeight = 0.0f;

	UPROPERTY(EditAnywhere, Category = "Mantle System")
		float LowPlayRate = 0.0f;

	UPROPERTY(EditAnywhere, Category = "Mantle System")
		float LowStartPosition = 0.0f;

	UPROPERTY(EditAnywhere, Category = "Mantle System")
		float HighHeight = 0.0f;

	UPROPERTY(EditAnywhere, Category = "Mantle System")
		float HighPlayRate = 0.0f;

	UPROPERTY(EditAnywhere, Category = "Mantle System")
		float HighStartPosition = 0.0f;
};

USTRUCT(BlueprintType)
struct FMantleParams
{
	GENERATED_BODY()

		UPROPERTY(EditAnywhere, Category = "Mantle System")
		UAnimMontage* AnimMontage = nullptr;

	UPROPERTY(EditAnywhere, Category = "Mantle System")
		UCurveVector* PositionCorrectionCurve = nullptr;

	UPROPERTY(EditAnywhere, Category = "Mantle System")
		float StartingPosition = 0.0f;

	UPROPERTY(EditAnywhere, Category = "Mantle System")
		float PlayRate = 0.0f;

	UPROPERTY(EditAnywhere, Category = "Mantle System")
		FVector StartingOffset;
};

USTRUCT(BlueprintType)
struct FMantleTraceSettings
{
	GENERATED_BODY()

		UPROPERTY(EditAnywhere, Category = "Mantle System")
		float MaxLedgeHeight = 0.0f;

	UPROPERTY(EditAnywhere, Category = "Mantle System")
		float MinLedgeHeight = 0.0f;

	UPROPERTY(EditAnywhere, Category = "Mantle System")
		float ReachDistance = 0.0f;

	UPROPERTY(EditAnywhere, Category = "Mantle System")
		float ForwardTraceRadius = 0.0f;

	UPROPERTY(EditAnywhere, Category = "Mantle System")
		float DownwardTraceRadius = 0.0f;
};

USTRUCT(BlueprintType)
struct FMovementSettings
{
	GENERATED_BODY()

		UPROPERTY(EditAnywhere, Category = "Movement Settings")
		float WalkSpeed = 0.0f;

	UPROPERTY(EditAnywhere, Category = "Movement Settings")
		float RunSpeed = 0.0f;

	UPROPERTY(EditAnywhere, Category = "Movement Settings")
		float SprintSpeed = 0.0f;

	UPROPERTY(EditAnywhere, Category = "Movement Settings")
		UCurveVector* MovementCurve = nullptr;

	UPROPERTY(EditAnywhere, Category = "Movement Settings")
		UCurveFloat* RotationRateCurve = nullptr;

	float GetSpeedForGait(const EGait Gait) const
	{
		switch (Gait)
		{
		case EGait::Running:
			return RunSpeed;
		case EGait::Sprinting:
			return SprintSpeed;
		case EGait::Walking:
			return WalkSpeed;
		default:
			return RunSpeed;
		}
	}
};

USTRUCT(BlueprintType)
struct FMovementStanceSettings
{
	GENERATED_BODY()

		UPROPERTY(EditAnywhere, Category = "Movement Settings")
		FMovementSettings Standing;

	UPROPERTY(EditAnywhere, Category = "Movement Settings")
		FMovementSettings Crouching;
};

USTRUCT(BlueprintType)
struct FMovementStateSettings : public FTableRowBase
{
	GENERATED_BODY()

		UPROPERTY(EditAnywhere, Category = "Movement Settings")
		FMovementStanceSettings VelocityDirection;

	UPROPERTY(EditAnywhere, Category = "Movement Settings")
		FMovementStanceSettings LookingDirection;

	UPROPERTY(EditAnywhere, Category = "Movement Settings")
		FMovementStanceSettings Aiming;
};


















USTRUCT(BlueprintType)
struct FHitFX : public FTableRowBase
{
	GENERATED_BODY()

		UPROPERTY(EditAnywhere, Category = "Surface")
		TEnumAsByte<enum EPhysicalSurface> SurfaceType;

	UPROPERTY(EditAnywhere, Category = "Sound")
		TSoftObjectPtr<USoundBase> Sound;

	UPROPERTY(EditAnywhere, Category = "Sound")
		ESpawnType SoundSpawnType;

	UPROPERTY(EditAnywhere, Category = "Sound", meta = (EditCondition = "SoundSpawnType == ESpawnType::Attached"))
		TEnumAsByte<enum EAttachLocation::Type> SoundAttachmentType;

	UPROPERTY(EditAnywhere, Category = "Sound")
		FVector SoundLocationOffset;

	UPROPERTY(EditAnywhere, Category = "Sound")
		FRotator SoundRotationOffset;

	UPROPERTY(EditAnywhere, Category = "Decal")
		TSoftObjectPtr<UMaterialInterface> DecalMaterial;

	UPROPERTY(EditAnywhere, Category = "Decal")
		ESpawnType DecalSpawnType;

	UPROPERTY(EditAnywhere, Category = "Decal", meta = (EditCondition = "DecalSpawnType == ESpawnType::Attached"))
		TEnumAsByte<enum EAttachLocation::Type> DecalAttachmentType;

	UPROPERTY(EditAnywhere, Category = "Decal")
		float DecalLifeSpan = 10.0f;

	UPROPERTY(EditAnywhere, Category = "Decal")
		FVector DecalSize;

	UPROPERTY(EditAnywhere, Category = "Decal")
		FVector DecalLocationOffset;

	UPROPERTY(EditAnywhere, Category = "Decal")
		FRotator DecalRotationOffset;

	UPROPERTY(EditAnywhere, Category = "Niagara")
		TSoftObjectPtr<UNiagaraSystem> NiagaraSystem;

	UPROPERTY(EditAnywhere, Category = "Niagara")
		ESpawnType NiagaraSpawnType;

	UPROPERTY(EditAnywhere, Category = "Niagara", meta = (EditCondition = "NiagaraSpawnType == ESpawnType::Attached"))
		TEnumAsByte<enum EAttachLocation::Type> NiagaraAttachmentType;

	UPROPERTY(EditAnywhere, Category = "Niagara")
		FVector NiagaraLocationOffset;

	UPROPERTY(EditAnywhere, Category = "Niagara")
		FRotator NiagaraRotationOffset;
};