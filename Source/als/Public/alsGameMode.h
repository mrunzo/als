// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "alsGameMode.generated.h"

UCLASS(minimalapi)
class AalsGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AalsGameMode();
};



